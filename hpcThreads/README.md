# TP de HPC sur les Threads C++

## Hello

- pour tester les threads C++

```
$ nix-shell --run hpcThreadsHello.py 
Bonjour, je suis le thread 140079549934464
Bonjour, je suis le thread 140079398782720
Bonjour, je suis le thread 140079390390016
```

## Fibo

- vérifier les résultats des différentes parallélisations :

```
$ nix-shell --run "hpcThreadsFiboTest.py 20"
fiboSequentiel:
 [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 13, 5, 18, 23, 41, 22, 21, 1, 22, 23]
fiboBlocs:
 [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 13, 5, 18, 23, 41, 22, 21, 1, 22, 23]
fiboCyclique2:
 [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 13, 5, 18, 23, 41, 22, 21, 1, 22, 23]
fiboCycliqueN (8):
 [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 13, 5, 18, 23, 41, 22, 21, 1, 22, 23]

```

- comparer les temps des différentes parallélisations :

```
$ nix-shell --run "hpcThreadsFiboTime.py 30000"
sequentiel: 1.8934495449066162 s
blocs: 1.3450028896331787 s
cyclique2: 0.8957140445709229 s
cycliqueN (8): 0.47682809829711914 s
```

- tracer les temps d'exécution/accélération/passage à l'échelle :

```
$ nix-shell --run hpcThreadsFiboPlot.py
1000 data on 1 proc in 0.002119302749633789 s
...

$ eog out_*.png
```

![](archives/out_times.png)
![](archives/out_speedups.png)
![](archives/out_scalability.png)

