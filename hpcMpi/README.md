# TP de HPC sur MPI en Python

## Hello1 et Hello2

idem que pour MPI en C++, mais en Python 

```
$ nix-shell --run "mpirun -n 4 hpcMpiHello1.py"
Hello, I am process #0
Hello, I am process #1
Hello, I am process #2
Hello, I am process #3
...
```


## Integral

Calcul d'intégrales par la méthode des rectangles. Approximation de pi.

Parallélisation via une réduction.

- version séquentielle :

```
$ nix-shell --run "hpcMpiIntegralSeq.py 0.0001"
0.0001 1 3.14157849407382 0.3852391242980957
```

- version parallèle MPI :

```
$ nix-shell --run "mpirun -n 4 hpcMpiIntegralPar.py 0.0001"
0.0001 4 3.1415884871165436 0.16899800300598145
```

- passage à l'échelle :

```
$ nix-shell --run hpcMpiIntegralRun.sh
$ nix-shell --run "hpcMpiPlot.py outIntegral.csv scalabilityIntegral.png"
$ eog scalabilityIntegral.png
```

TODO
![](archives/outIntegral.png)


## Image

Calcule une image floutée (convolution 2D).

Parallélisation via une collecte (gather).

![](archives/backloop.png)

![](archives/backloop_blur.png)

- version séquentielle :

```
$ nix-shell --run "hpcMpiImageSeq.py 20"
20 1 4.12873649597168
```

- version parallèle MPI :

```
$ nix-shell --run "mpirun -n 4 hpcMpiImagePar.py 20"
20 4 1.0679895329994906
```

- passage à l'échelle :

```
$ nix-shell --run hpcMpiImageRun.sh
$ nix-shell --run "hpcMpiPlot.py outImage.csv scalabilityImage.png"
$ eog scalabilityImage.png
```

TODO
![](archives/outImage.png)

